"use strict"

const dedent = require("dedent")
const expect = require("chai").expect
const parser = require("../../lib/parser")

describe("Parsing a method test:", () => {
    let parse = (input) =>
        parser.parse(dedent`
            package foo
            
            class Foo`
            + dedent("|\n" + input).replace(/^.*\n/, "").replace(/\n|^/g, "\n    ")
        ).declarations[0].methods

    it("A method without parameters and returned type", () => {
        let input = `
            method bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with generic parameters", () => {
        let input = `
            method bar<P, Q, R>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [],
            genericParameters: [
                {
                    nodeType: "parameter",
                    name: "P"
                },
                {
                    nodeType: "parameter",
                    name: "Q"
                },
                {
                    nodeType: "parameter",
                    name: "R"
                }
            ],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with description", () => {
        let input = `
            method bar
                """
                The bar method description.
                """
        `
        
        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "The bar method description.",
            annotations: [],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with multiline description", () =>{
        let input = `
            method bar
                """
                First line
                Second line
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "First line\nSecond line",
            annotations: [],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with an annotation", () =>{
        let input = `
            @Annotation
            method bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with an annotation in one line", () =>{
        let input = `
            @Annotation method bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with two annotations", () =>{
        let input = `
            @BazAnnotation
            @QuxAnnotation
            method bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "BazAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "QuxAnnotation"
                }
            ],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("A method with two annotations in one line", () =>{
        let input = `
            @BazAnnotation @QuxAnnotation method bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "BazAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "QuxAnnotation"
                }
            ],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "none",
                genericArguments: []
            }
        }])
    })

    it("Two methods", () => {
        let input = `
            method bar
            method baz

            method qux
        `

        expect(parse(input))
        .to.be.deep.equal([
            {
                nodeType: "method",
                name: "bar",
                description: "",
                annotations: [],
                genericParameters: [],
                parameters: [],
                returns: {
                    nodeType: "type",
                    name: "none",
                    genericArguments: []
                }
            },
            {
                nodeType: "method",
                name: "baz",
                description: "",
                annotations: [],
                genericParameters: [],
                parameters: [],
                returns: {
                    nodeType: "type",
                    name: "none",
                    genericArguments: []
                }
            },
            {
                nodeType: "method",
                name: "qux",
                description: "",
                annotations: [],
                genericParameters: [],
                parameters: [],
                returns: {
                    nodeType: "type",
                    name: "none",
                    genericArguments: []
                }
            }
        ])
    })

    it("A method which returns a type", () => {
        let input = `
            method bar: Baz
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "Baz",
                genericArguments: []
            }
        }])
    })

    it("A method which returns a type with generic arguments", () => {
        let input = `
            method bar: Baz<Qux, Duck>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "Baz",
                genericArguments: [
                    {
                        nodeType: "type",
                        name: "Qux",
                        genericArguments: []
                    },
                    {
                        nodeType: "type",
                        name: "Duck",
                        genericArguments: []
                    }
                ]
            }
        }])
    })

    it("A method which returns a type with a namespace", () => {
        let input = `
            method bar: baz.qux.Duck
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "method",
            name: "bar",
            description: "",
            annotations: [],
            genericParameters: [],
            parameters: [],
            returns: {
                nodeType: "type",
                name: "baz.qux.Duck",
                genericArguments: []
            }
        }])
    })
})

