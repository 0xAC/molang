"use strict"

const dedent = require("dedent")
const expect = require("chai").expect
const parser = require("../../lib/parser")

describe("Parsing a class dependency test:", () => {
    let parse = (input) =>
        parser.parse(dedent`
            package foo
            
            class Foo`
            + dedent("|\n" + input).replace(/^.*\n/, "").replace(/\n|^/g, "\n    ")
        ).declarations[0].dependencies

    it("A dependency without a type", () => {
        let input = `
            dependency bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "any",
                "nodeType": "type"
            }
        }])
    })

    it("A dependencies with a type", () => {
        let input = `
            dependency bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A dependencies with a type that have generic arguments", () => {
        let input = `
            dependency bar: Qux<Corge, Grault>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [],
            type: {
                "genericArguments": [
                    {
                        nodeType: "type",
                        name: "Corge",
                        genericArguments: []
                    },
                    {
                        nodeType: "type",
                        name: "Grault",
                        genericArguments: []
                    }
                ],
                name: "Qux",
                nodeType: "type"
            }
        }])
    })

    it("A dependency with a two line description", () => {
        let input = `
            dependency bar: Qux
                """
                One line description
                Second line of description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "One line description\nSecond line of description",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A dependency with a description", () => {
        let input = `
            dependency bar: Qux
                """
                One line description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "One line description",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A dependency with annotation", () => {
        let input = `
            @Annotation
            dependency bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A dependency with two annotations", () => {
        let input = `
            @CorgeAnnotation
            @GraultAnnotation
            dependency bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "CorgeAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "GraultAnnotation"
                }
            ],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A dependency with one line annotation", () => {
        let input = `
            @CorgeAnnotation dependency bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "CorgeAnnotation"
            }],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A dependency with two one line annotations", () => {
        let input = `
            @CorgeAnnotation @GraultAnnotation dependency bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "dependency",
            name: "bar",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "CorgeAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "GraultAnnotation"
                }
            ],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("Multiple dependencies", () => {
        let input = `
            dependency baz: Qux
            dependency corge: Qux

            dependency grault: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([
            {
                nodeType: "dependency",
                name: "baz",
                description: "",
                annotations: [],
                type: {
                    "genericArguments": [],
                    "name": "Qux",
                    "nodeType": "type"
                }
            },
            {
                nodeType: "dependency",
                name: "corge",
                description: "",
                annotations: [],
                type: {
                    "genericArguments": [],
                    "name": "Qux",
                    "nodeType": "type"
                }
            },
            {
                nodeType: "dependency",
                name: "grault",
                description: "",
                annotations: [],
                type: {
                    "genericArguments": [],
                    "name": "Qux",
                    "nodeType": "type"
                }
            },
        ])
    })
})
