"use strict"

const dedent = require("dedent")
const expect = require("chai").expect
const parser = require("../../lib/parser")

describe("Parsing a class test:", () => {
    let parse = (input) =>
        parser.parse(dedent`package foo\n${ input }`).declarations
        

    it("An empty class", () => {
        let input = `
            class Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with generic parameters", () => {
        let input = `
            class Foo<P, Q, R>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [],
            genericParameters: [
                {
                    nodeType: "parameter",
                    name: "P"
                },
                {
                    nodeType: "parameter",
                    name: "Q"
                },
                {
                    nodeType: "parameter",
                    name: "R"
                }
            ],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with description", () => {
        let input = `
            class Foo
                """
                Foo class description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "Foo class description",
            annotations: [],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with multiline description", () => {
        let input = `
            class Foo
                """
                Foo class description
                this is another line of the description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "Foo class description\nthis is another line of the description",
            annotations: [],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class which implements an interface", () => {
        let input = `
            class Foo implements Bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [{
                nodeType: "type",
                name: "Bar",
                genericArguments: []
            }],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class which implements many interfaces", () => {
        let input = `
            class Foo implements Bar, Baz
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [
                {
                    nodeType: "type",
                    name: "Bar",
                    genericArguments: []
                },
                {
                    nodeType: "type",
                    name: "Baz",
                    genericArguments: []
                }
            ],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class which implements an interface with a namespace", () => {
        let input = `
            class Foo implements bar.baz.Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [{
                nodeType: "type",
                name: "bar.baz.Qux",
                genericArguments: []
            }],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class which implements an interface with generic arguments", () => {
        let input = `
            class Foo implements Bar<Baz, Qux>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [{
                nodeType: "type",
                name: "Bar",
                genericArguments: [
                    {
                        nodeType: "type",
                        name: "Baz",
                        genericArguments: []
                    },
                    {
                        nodeType: "type",
                        name: "Qux",
                        genericArguments: []
                    },
                ]
            }],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with an annotation", () => {
        let input = `
            @Annotation
            class Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with an annotation in one line", () => {
        let input = `
            @Annotation class Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with two annotations", () => {
        let input = `
            @BarAnnotation
            @BazAnnotation
            class Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "BarAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "BazAnnotation"
                }
            ],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("A class with two annotations in one line", () => {
        let input = `
            @BarAnnotation @BazAnnotation class Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "class",
            name: "Foo",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "BarAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "BazAnnotation"
                }
            ],
            superTypes: [],
            genericParameters: [],
            methods: [],
            dependencies: []
        }])
    })

    it("Two classes", () => {
        let input = `
            class Foo
            class Bar

            class Baz
        `

        expect(parse(input))
        .to.be.deep.equal([
            {
                nodeType: "class",
                name: "Foo",
                description: "",
                annotations: [],
                superTypes: [],
                genericParameters: [],
                methods: [],
                dependencies: []
            },
            {
                nodeType: "class",
                name: "Bar",
                description: "",
                annotations: [],
                superTypes: [],
                genericParameters: [],
                methods: [],
                dependencies: []
            },
            {
                nodeType: "class",
                name: "Baz",
                description: "",
                annotations: [],
                superTypes: [],
                genericParameters: [],
                methods: [],
                dependencies: []
            }
        ])
    })
})
