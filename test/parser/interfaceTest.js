"use strict"

const dedent = require("dedent")
const expect = require("chai").expect
const parser = require("../../lib/parser")

describe("Parsing an interface test:", () => {
    let parse = (input) =>
        parser.parse(dedent`package foo\n${ input }`).declarations
        

    it("An empty interface", () => {
        let input = `
            interface Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface with generic parameters", () => {
        let input = `
            interface Foo<P, Q, R>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [],
            genericParameters: [
                {
                    nodeType: "parameter",
                    name: "P"
                },
                {
                    nodeType: "parameter",
                    name: "Q"
                },
                {
                    nodeType: "parameter",
                    name: "R"
                }
            ],
            methods: []
        }])
    })

    it("An interface with description", () => {
        let input = `
            interface Foo
                """
                Foo interface description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "Foo interface description",
            annotations: [],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface with multiline description", () => {
        let input = `
            interface Foo
                """
                Foo interface description
                    this is another line of the description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "Foo interface description\nthis is another line of the description",
            annotations: [],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface which extends another interface", () => {
        let input = `
            interface Foo extends Bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [{
                nodeType: "type",
                name: "Bar",
                genericArguments: []
            }],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface which extends many interfaces", () => {
        let input = `
            interface Foo extends Bar, Baz
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [
                {
                    nodeType: "type",
                    name: "Bar",
                    genericArguments: []
                },
                {
                    nodeType: "type",
                    name: "Baz",
                    genericArguments: []
                }
            ],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface which extends an interface with a namespace", () => {
        let input = `
            interface Foo extends bar.baz.Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [{
                nodeType: "type",
                name: "bar.baz.Qux",
                genericArguments: []
            }],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface which extends another interface with generic arguments", () => {
        let input = `
            interface Foo extends Bar<Baz, Qux>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [],
            superTypes: [{
                nodeType: "type",
                name: "Bar",
                genericArguments: [
                    {
                        nodeType: "type",
                        name: "Baz",
                        genericArguments: []
                    },
                    {
                        nodeType: "type",
                        name: "Qux",
                        genericArguments: []
                    },
                ]
            }],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface with annotation", () => {
        let input = `
            @Annotation
            interface Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface with an annotation in one line", () => {
        let input = `
            @Annotation interface Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface with two annotations", () => {
        let input = `
            @BarAnnotation
            @BazAnnotation
            interface Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "BarAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "BazAnnotation"
                }
            ],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("An interface with two annotations in one line", () => {
        let input = `
            @BarAnnotation @BazAnnotation interface Foo
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "interface",
            name: "Foo",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "BarAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "BazAnnotation"
                }
            ],
            superTypes: [],
            genericParameters: [],
            methods: []
        }])
    })

    it("Two interfaces", () => {
        let input = `
            interface Foo
            interface Bar

            interface Baz
        `

        expect(parse(input))
        .to.be.deep.equal([
            {
                nodeType: "interface",
                name: "Foo",
                description: "",
                annotations: [],
                superTypes: [],
                genericParameters: [],
                methods: []
            },
            {
                nodeType: "interface",
                name: "Bar",
                description: "",
                annotations: [],
                superTypes: [],
                genericParameters: [],
                methods: []
            },
            {
                nodeType: "interface",
                name: "Baz",
                description: "",
                annotations: [],
                superTypes: [],
                genericParameters: [],
                methods: []
            }
        ])
    })
})
