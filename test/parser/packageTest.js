"use strict"

const dedent = require("dedent")
const expect = require("chai").expect
const parser = require("../../lib/parser")

describe("Parsing a package test:", () => {
    it("Empty package", () => {
        let input = dedent`
            package foo
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: []
        })
    })

    it("Package with a complex name", () => {
        let input = dedent`
            package foo.bar.baz
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo.bar.baz",
            declarations: [],
            imports: []
        })
    })

    it("Package with an import", () => {
        let input = dedent`
            package foo

            import bar.Baz
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [{
                nodeType: "import",
                name: "bar.Baz",
                alias: "Baz"
            }]
        })
    })

    it("Package with multiple, multiline imports", () => {
        let input = dedent`
            package foo

            import bar.Baz
            import qux.Qux
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Baz"
                },
                {
                    nodeType: "import",
                    name: "qux.Qux",
                    alias: "Qux"
                }
            ]
        })
    })

    it("Package with multiple, oneline imports", () => {
        let input = dedent`
            package foo

            import bar.Baz, qux.Qux
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Baz"
                },
                {
                    nodeType: "import",
                    name: "qux.Qux",
                    alias: "Qux"
                }
            ]
        })
    })

    it("Package with an aliased import", () => {
        let input = dedent`
            package foo

            import bar.Baz as Qux
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Qux"
                }
            ]
        })
    })

    it("Package with multiple imports, where some of them are aliased", () => {
        let input = dedent`
            package foo

            import bar.Baz, goo.Rot as Qux, hua.Yep as Duck
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Baz"
                },
                {
                    nodeType: "import",
                    name: "goo.Rot",
                    alias: "Qux"
                },
                {
                    nodeType: "import",
                    name: "hua.Yep",
                    alias: "Duck"
                }
            ]
        })
    })

    it("Package with an import from", () => {
        let input = dedent`
            package foo
    
            from bar import Baz
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Baz"
                }
            ]
        })
    })

    it("Package with an import from, where the package name is multipart", () => {
        let input = dedent`
            package foo
    
            from bar.qux import Qux
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.qux.Qux",
                    alias: "Qux"
                }
            ]
        })
    })

    it("Package with multiple imports from", () => {
        let input = dedent`
            package foo
    
            from bar import Baz, Qux
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Baz"
                },
                {
                    nodeType: "import",
                    name: "bar.Qux",
                    alias: "Qux"
                }
            ]
        })
    })

    it("Package with an aliased import from", () => {
        let input = dedent`
            package foo
    
            from bar import Baz as Qux
        `

        expect(parser.parse(input))
        .to.be.deep.equal({
            nodeType: "package",
            name: "foo",
            declarations: [],
            imports: [
                {
                    nodeType: "import",
                    name: "bar.Baz",
                    alias: "Qux"
                }
            ]
        })
    })
})
