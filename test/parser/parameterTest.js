"use strict"

const dedent = require("dedent")
const expect = require("chai").expect
const parser = require("../../lib/parser")

describe("Parsing a method parameter test:", () => {
    let parse = (input) =>
        parser.parse(dedent`
            package foo
            
            class Foo
                method foo`
            + dedent("|\n" + input).replace(/^.*\n/, "").replace(/\n|^/g, "\n        ")
        ).declarations[0].methods[0].parameters

    it("A parameter without a type", () => {
        let input = `
            param bar
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "any",
                "nodeType": "type"
            }
        }])
    })

    it("A parameters with a type", () => {
        let input = `
            param bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A parameters with a type that have generic arguments", () => {
        let input = `
            param bar: Qux<Corge, Grault>
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [],
            type: {
                "genericArguments": [
                    {
                        nodeType: "type",
                        name: "Corge",
                        genericArguments: []
                    },
                    {
                        nodeType: "type",
                        name: "Grault",
                        genericArguments: []
                    }
                ],
                name: "Qux",
                nodeType: "type"
            }
        }])
    })

    it("A parameter with a two line description", () => {
        let input = `
            param bar: Qux
                """
                One line description
                Second line of description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "One line description\nSecond line of description",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A parameter with a description", () => {
        let input = `
            param bar: Qux
                """
                One line description
                """
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "One line description",
            annotations: [],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A parameter with annotation", () => {
        let input = `
            @Annotation
            param bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "Annotation"
            }],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A parameter with two annotations", () => {
        let input = `
            @CorgeAnnotation
            @GraultAnnotation
            param bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "CorgeAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "GraultAnnotation"
                }
            ],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A parameter with one line annotation", () => {
        let input = `
            @CorgeAnnotation param bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [{
                nodeType: "annotation",
                name: "CorgeAnnotation"
            }],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("A parameter with two one line annotations", () => {
        let input = `
            @CorgeAnnotation @GraultAnnotation param bar: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([{
            nodeType: "argument",
            name: "bar",
            description: "",
            annotations: [
                {
                    nodeType: "annotation",
                    name: "CorgeAnnotation"
                },
                {
                    nodeType: "annotation",
                    name: "GraultAnnotation"
                }
            ],
            type: {
                "genericArguments": [],
                "name": "Qux",
                "nodeType": "type"
            }
        }])
    })

    it("Multiple parameters", () => {
        let input = `
            param baz: Qux
            param corge: Qux

            param grault: Qux
        `

        expect(parse(input))
        .to.be.deep.equal([
            {
                nodeType: "argument",
                name: "baz",
                description: "",
                annotations: [],
                type: {
                    "genericArguments": [],
                    "name": "Qux",
                    "nodeType": "type"
                }
            },
            {
                nodeType: "argument",
                name: "corge",
                description: "",
                annotations: [],
                type: {
                    "genericArguments": [],
                    "name": "Qux",
                    "nodeType": "type"
                }
            },
            {
                nodeType: "argument",
                name: "grault",
                description: "",
                annotations: [],
                type: {
                    "genericArguments": [],
                    "name": "Qux",
                    "nodeType": "type"
                }
            },
        ])
    })
})
