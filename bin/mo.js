#!/usr/bin/env node

"use strict"

const _ = require("prelude-ls")
const fs = require("fs")
const http = require("http")
const path = require("path")
const coffee = require("../coffee")
const fileUtil = require("../lib/fileUtil")
const parser = require("../lib/parser")
const plantUmlUtil = require("../lib/plantUmlUtil")
const textUtil = require("../lib/textUtil")

var renderMethods = function(methods) {
    return methods
    .map(method => renderers.method(method))
    .reduce((prev, current) => prev.concat(current), [])
    .map(line => "    " + line)
}

var renderDependencies = function(dependencies) {
    return dependencies
    .map(dependency => renderers.dependency(dependency))
    .reduce((prev, current) => prev.concat(current), [])
    .map(line => "    " + line)
}

var renderType = function(type) {
    var result = type.name

    if (type.genericArguments.length !== 0) {
        result += "<" + type.genericArguments.map(argument => renderType(argument)).join(", ") + ">"
    }

    return result
}

var createTypeName = function(type) {
    var name = type.name
    
    if (type.genericParameters.length !== 0) {
        name += "<" + type.genericParameters.join(", ") + ">"
    }

    return name
}

var renderers = {
    "class": function(class_) {
        var lines = ["class " + createTypeName(class_) + " {"]
        var rawDescription = textUtil.wrapLines(class_.description, 50)

        var description = rawDescription !== "" ? rawDescription.split("\n") : []
        var dependencies = renderDependencies(class_.dependencies)
        var methods = renderMethods(class_.methods)

        if (description.length > 0) {
            description = description.map(line => "    " + line)
        }

        if (description.length > 0 && (dependencies.length > 0 || methods.length > 0)) {
            description.push("    ___")
        }

        if (dependencies.length > 0 && methods.length > 0) {
            dependencies.push("    ___")
        }

        if (dependencies.length === 0 && description.length === 0) {
            lines = ["hide " + class_.name + " fields"].concat(lines)
        }

        if (methods.length === 0) {
            lines = ["hide " + class_.name + " methods"].concat(lines)
        }

        return lines.concat(description).concat(dependencies).concat(methods).concat(["}"])
    },
    "interface": function(interface_) {
        var lines = ["interface " + createTypeName(interface_) + " {"]

        var description = interface_.description !== "" ? [interface_.description.replace(/^\s*|\s*$/g, "")] : []

        if (description.length > 0) {
            description[0] = description[0].replace(/\s+/g, " ")
            description[0] = "    " + description[0]
        }

        var body = renderMethods(interface_.methods)

        if (dependencies.length > 0 && body.length > 0) {
            description.push("    ___")
        }

        if (description.length === 0) {
            lines = ["hide " + interface_.name + " fields"].concat(lines)
        }

        if (body.length === 0) {
            lines = ["hide " + interface_.name + " methods"].concat(lines)
        }

        return lines.concat(description).concat(body).concat(["}"])
    },
    "dependency": function(dependency) {
        return ["- " + dependency.name + renderers["type"](dependency.type)]
    },
    "method": function(method) {
        var args = method.parameters
        .map(renderers["argument"])
        .join(", ")

        return ["+ " + method.name + "(" + args + ")"] + renderers["type"](method.returns)
    },
    "type": function(type) {
        return type.name === "none" ? "" : ": " + renderType(type)
    },
    "argument": function(argument) {
        return argument.name + renderers["type"](argument.type)
    }
}

var dependencies = {
    "class": function(class_) {
        return class_.dependencies
        .map(dependency => dependency.type.name)
        .reduce(function(prev, current) {
            if (prev.indexOf(current) === -1) {
                prev.push(current)
            }

            return prev
        }, [])
        .map(name => class_.name + " <-- " + name)
    }
}

var ancestors = {
    "class": function(class_) {
        return class_.superTypes
        .map(superType => superType.name)
        .map(function(ancestor) {
            return {
                name: class_.name,
                ancestor
            }
        })
        .reduce((prev, current) => prev.concat(current), [])
        .map(item => item.ancestor + " <|-- " + item.name)
    },
    "interface": function(interface_) {
        return interface_.superTypes
        .map(superType => superType.name)
        .map(function(ancestor) {
            return {
                name: interface_.name,
                ancestor
            }
        })
        .reduce((prev, current) => prev.concat(current), [])
        .map(item => item.ancestor + " <|-- " + item.name)
    }
}

var tableBuilder = {
    "class": function(symbolTable, class_) {
        symbolTable[class_.name] = class_
    },
    "interface": function(symbolTable, interface_) {
        symbolTable[interface_.name] = interface_
    }
}

var processDocument = function(documentRoot) {
    var symbolTable = {}

    documentRoot.declarations.forEach(function(item) {
        tableBuilder[item.nodeType](symbolTable, item)
    })

    var lines = _.concatMap(item => renderers[item.nodeType](item).join("\n"), documentRoot.declarations)

    var classes = documentRoot.declarations.filter(item => item.nodeType === "class")
    var interfaces = documentRoot.declarations.filter(item => item.nodeType === "interface")

    lines = lines.concat(_.concatMap(class_ => dependencies["class"](class_), classes))

    lines = lines.concat(
        _.concatMap(
            definition => ancestors[definition.nodeType](definition),
            documentRoot.declarations
        )
    )
    
    coffee(symbolTable, documentRoot)

    var diagramText = lines.join("\n")

    fileUtil.writeFile(`output${path.sep}diagram`, diagramText, "UTF-8")
    .then(() => {
        http.get("http://plantuml.com/plantuml/svg/" + plantUmlUtil.encode(diagramText), function(response) {
            response.pipe(fs.createWriteStream(`output${path.sep}diagram.svg`))
        })
    })
}

var parseDocument = function(documentText) {
    return parser.parse(documentText.replace(/\s+$/g, ""))
}

Promise.all(
    process.argv
    .filter(sourceFilePath => /\.schm$/.test(sourceFilePath))
    .map(sourceFilePath => fileUtil.readFile(sourceFilePath, "UTF-8"))
)
.then(_.map(parseDocument))
.then(_.map(processDocument))
.catch(function(error) {
    if (error.name === "SyntaxError") {
        console.error(
            "SyntaxError at " +
            error.location.start.line + "," + error.location.start.column + ": " +
            error.message)
        return
    }

    console.error(error.stack)
})
