" Vim syntax file
" Language: Custom Model
" Maintainer: Adam Chrapkowski

if exists("b:current_syntax")
  finish
endif

syn keyword top class interface package import from as
syn keyword inner method dependency param
syn keyword operator implements extends
syn match annotation '@[A-Z][A-Za-z0-9]*'
syn match type '\<[A-Z][A-Za-z0-9]*\>' skipwhite
syn region description start=/\v"""/ skip=/\v\\./ end=/\v"""/

hi def link inner Statement
hi def link top Constant
hi def link operator PreProc
hi def link type Type
hi def link description Include
hi def link annotation Identifier
