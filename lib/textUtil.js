"use strict"

exports.wrapLines = function(text, limit) {
    var rawText = text.replace(/\s*\n+\s*/g, " ").replace(/^\s*|\s*$/g, "")
    
    var lines = []
    rawText.split(/\s+/).forEach(function(word) {
        var line;

        if (lines.length > 0) { 
            line = lines[lines.length - 1]
        } else {
            line = ""
            lines.push(line)
        }

        if (line.length + word.length > limit) {
            line = ""
            lines.push(line)
        }

        line += " " + word

        lines[lines.length - 1] = line.replace(/^\s*/, "")
    })

    return lines.join("\n")
}

exports.trimArgumentsList = function(text) {
    return text.replace(/(,\s*$)/g, "")
}

exports.capitalizeFirstLetter = function(text) {
    return text.slice(0, 1).toUpperCase() + text.slice(1)
}
