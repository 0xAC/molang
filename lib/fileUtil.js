"use strict"

const fs = require("fs")
const path = require("path")
const _ = require("prelude-ls")

var promiseCallback = function(fulfill, reject) {
    return function (error, value) {
        if (error) {
            reject(error)
        } else {
            fulfill(value)
        }
    }
}

exports.listDirectory = function(directoryPath) {
    return new Promise(
        (fulfill, reject) => fs.readdir(directoryPath, promiseCallback(fulfill, reject))
    )
    .then(files => Promise.all(files.map(nodeName =>
        new Promise(
            (fulfill, reject) =>
                fs.stat(directoryPath + path.sep + nodeName, promiseCallback(fulfill, reject))
        )
        .then(function(stats) { 
            return {
                path: directoryPath + path.sep + nodeName,
                isDirectory: stats.isDirectory(),
                isFile: stats.isFile()
            }
        })
    )))
    .then(function(nodes) {
        var files = nodes
        .filter(node => node.isFile)
        .map(node => node.path)
        .sort()

        var directoriesPromises = nodes
        .filter(node => node.isDirectory)
        .map(node => node.path)
        .sort()
        .map(nodePath => listDirectory(nodePath))

        return Promise.all([files].concat(directoriesPromises))
    })
    .then(listOfFileLists => Array.prototype.concat.apply([], listOfFileLists))
}

exports.readFile = function(file, options) {
    return new Promise(
        (fulfill, reject) => fs.readFile(file, options, promiseCallback(fulfill, reject))
    )
}

exports.writeFile = function(file) {
    // TODO: Use ES6 rest parameter when it works on node be default
    // or when we use Babel
    let args = Array.prototype.slice.call(arguments, 1) 

    if (file.indexOf(path.sep) !== -1) {
        let pathSegments = file.split(path.sep).slice(0, -1)
        createDirectorySegment(_.head(pathSegments), _.tail(pathSegments))
    }
    
    return new Promise(
        (fulfill, reject) => fs.writeFile(file, ...args, promiseCallback(fulfill, reject))
    )
}

let createDirectorySegment = function(base, pathSegments) {
    if (!fs.existsSync(base)) {
        fs.mkdirSync(base)
    }

    if (pathSegments.length > 0) {
        base += path.sep + pathSegments.shift()
        createDirectorySegment(base, pathSegments)
    }
}

exports.createDirectory = function(directoryPath) {
    let directorySegments = directoryPath.split(path.sep)
    createDirectorySegment(_.head(directorySegments), _.tail(directorySegments))
}
