"use strict"

const deflate = require("deflate-js")
const _ = require("prelude-ls")

var limitAndOffsetPairs = [
    [10, 48],
    [26, 65],
    [26, 97]
]

var encode6Bit = function(byte) {
    for (let pair of limitAndOffsetPairs) {
        let limit = pair[0]
        let offset = pair[1]

        if (byte < limit) {
            return offset + byte
        }
        byte -= limit
    }

    switch (byte) {
        case 0: return 45
        case 1: return 95
        default: return 63
    }
}

let extend3BytesTo4Bytes = bytes =>
    [
        bytes[0] >> 2,
        (bytes[0] & 0x3) << 4 | bytes[1] >> 4,
        (bytes[1] & 0xF) << 2 | bytes[2] >> 6,
        bytes[2]
    ]
    .map(byte => byte & 0x3F)
    .map(encode6Bit)
    .map(charCode => String.fromCharCode(charCode))

let chunkGenerator = function*(array) {
    for (let bytes = array; bytes.length > 0; bytes = _.drop(3, bytes)) {
        let triple = _.take(3, bytes)
        if (triple.length < 3) {
            triple = triple.concat(_.replicate(3 - triple.length, 0))
        }

        for (let byte of extend3BytesTo4Bytes(triple)) {
            yield byte
        }
    }
}

var compress = (text) =>
    deflate.deflate(_.map(char => char.charCodeAt(0), text.toString("utf8")), 9)

exports.encode = text => [...chunkGenerator(compress(text))].join("")
