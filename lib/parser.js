"use strict"

const fs = require("fs")
const path = require("path")
const peg = require("pegjs")

module.exports = peg.buildParser(
    fs.readFileSync(path.resolve(__dirname, "../syntax.pegjs"), "UTF-8"))



