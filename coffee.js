"use strict"

const path = require("path")
const fs = require("fs")
const util = require("util")
const mustache = require("mustache")
const textUtil = require("./lib/textUtil")
const fileUtil = require("./lib/fileUtil")
const _ = require("prelude-ls")

mustache.escape = value => value

var generateMethods = methods => _.uniqueBy(it => it.name, methods.map(function(method) {
    return {
        name: method.name,
        args: generateArguments(method.parameters),
        description: method.description
    }
}))

var generateDecription = function(description, limit, indent) {
    if (description.length === 0) description = "TODO: Add description"

    var lineBreak = "\n" + _.repeat(indent, " ") + "# "

    return textUtil.wrapLines(description, limit - indent).replace(/\n/g, lineBreak)
}

var generateArguments = parameters => parameters.map(function(arg) {
    return {
        name: arg.name,
        type: arg.type.name,
        description: arg.description
    }
})

var findType = function(symbolTable, name) {
    if (!(name in symbolTable)) {
        throw new Error("Cannot find type: " + name)
    }
    return symbolTable[name]
}

var collectMethods = function(symbolTable, typeDefinition) {
    var ancestorsMethods = _.concatMap(
        ancestor => collectMethods(symbolTable, ancestor),
        typeDefinition.superTypes.map(superType => findType(symbolTable, superType.name)))
    
    return typeDefinition.methods.concat(ancestorsMethods)
}

var generateCommonView = function(packageName, type, symbolTable) {
    return {
        comment: () => (text, render) => generateDecription(render(text), 110, 4),
        description: type.description,
        package: packageName.replace(/\./g, "/") + "/" + type.name,
        className: type.name,
        hasMethods: type.methods.length !== 0,
        class: type,
        isClass: type.nodeType === "class",
        trim: function () {
            return (text, render) => textUtil.trimArgumentsList(render(text))
        },
        capitalize: function() {
            return (text, render) => textUtil.capitalizeFirstLetter(render(text))
        }
    }
}

var generateClassView = function(packageName, class_, symbolTable) {
    return {
        methods: generateMethods(collectMethods(symbolTable, class_)),
        hasConstructor: class_.dependencies.length !== 0,
        validators: _.unique(class_.dependencies.map(dependency => dependency.name)
            .concat(_.concatMap(method => _.concatMap(param => param.name, method.parameters), class_.methods))),
        dependencies: generateArguments(class_.dependencies),
        hasArgs: class_.dependencies.length !== 0
    }
}

var generateInterfaceView = function(packageName, interface_, symbolTable) {
    return {
        methods: generateMethods(interface_.methods),
        hasConstructor: false,
        validators: [],
        dependencies: [],
        hasArgs: false
    }
}

var generateTypeView = function(packageName, type, symbolTable) {
    return Object.assign(
        {},
        generateCommonView(packageName, type, symbolTable),
        type.nodeType === "class" ? generateClassView(packageName, type, symbolTable) : generateInterfaceView(packageName, type, symbolTable)
    )
}


var pairs = [
    [path.resolve(__dirname, "template.mst"), "output/coffee"],
    [path.resolve(__dirname, "test.mst"), "output/test", member => member.nodeType === "class"]
]

module.exports = function(symbolTable, root) {
    pairs.forEach(function(tuple) {
        var templateFile = tuple[0]
        var outputDirectory = tuple[1]
        var predicate = tuple.length < 3 ? (member => true) : tuple[2]

        fileUtil.readFile(templateFile, "UTF-8")
        .then(function(classTemplate) {
            root.declarations.filter(predicate).forEach(function(declaration) {
                var moduleContent = mustache.render(
                    classTemplate,
                    generateTypeView(root.name, declaration, symbolTable)
                )

                var directoryPath = outputDirectory + path.sep + root.name.replace(/\./g, path.sep)
                fileUtil.createDirectory(directoryPath)

                fs.writeFile(directoryPath + path.sep + declaration.name + ".coffee", moduleContent, "UTF-8", () => {})
            })
        })
        .catch(error => console.error(error.stack))
    })
}
