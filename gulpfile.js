"use strict"

let gulp = require("gulp")
let mocha = require("gulp-mocha")

gulp.task("mocha", () => 
    gulp.src("test/**/*.js", { read: false })
    .pipe(mocha({ reporter: "nyan" }))
)

gulp.task("mocha-watch", () => {
    gulp.watch(["test/**"], ["mocha"])
})

