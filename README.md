# Install

    npm install -g .

or directly from git

    npm install https://0xAC@bitbucket.org/0xAC/molang.git#master -g

# Usage

    mo myfile.schm

# Syntax of schm files

Molang is a really simple prototyping language which allows you to define
interfaces, classes and packages. Output of the language itself is very simple
abstract syntax tree which can be used to represent whatever you want.

Below you can find desciption of the syntax.

Each Molang source file must start with a package declaration.

    package foo

Inside a package it is possible to declare any number of classes and interfaces.

    package foo

    class Foo

    class Bar

    interface Qux

A class definition consists of its dependencies and methods.

    class Foo
        dependency bar: Bar
        dependency baz: Baz

        method sayHello

An interface definition may contains only methods.

    interface Bar
        method sayHello

A method may return a type and have a parameters list.

    interface Bar
        method add: Number
            param firstOperand: Number
            param secondOperand: Number

Interfaces may extend other interfaces:

    interface Foo
        method read

    interface Bar extends Foo
        method listen

Interface may extend multiple interfaces:

    interface Foo
        method read
    
    interface Bar
        method listen

    interface Baz extends Foo, Bar

Classes may implement interfaces:

    interface Foo
        method read

    class Bar implements Foo

It is not possible to extend class or have an abstract class. Molang prefers
composition over inheritance.

If you need to add some extra information to classes, interfaces, etc.
You may find annotations mechanism quite useful.

    @Serializable
    class Bar

    interface Baz
        method computeSalary
            @Nullable param employee: Emplyee

Annotations are just labels which can be added to statements in order
to add some meta information to them.
