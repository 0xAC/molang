{
    "use strict"

    const _ = require("prelude-ls")
    const dedent = require("dedent")

    let declarationNodeTypes = ["class", "interface"]

    let nodeFactory = {
        createClass: (name, members, superTypes, description, genericParameters, annotations) => {
            return {
                nodeType: "class",
                name,
                methods: members.filter(member => member.nodeType === "method"),
                dependencies: members.filter(member => member.nodeType === "dependency"),
                superTypes,
                description: dedent(description),
                genericParameters,
                annotations
            }
        },
        createInterface: (name, methods, superTypes, description, genericParameters, annotations) => (
            {
                nodeType: "interface",
                name,
                methods,
                superTypes,
                description: dedent(description),
                genericParameters,
                annotations
            }
        ),
        createDependency: (name, type, description, annotations) => (
            {
                nodeType: "dependency",
                name,
                type,
                description: dedent(description),
                annotations
            }
        ),
        createMethod: (name, parameters, returns, description, genericParameters, annotations) => (
            {
                nodeType: "method",
                name,
                parameters,
                returns,
                description: dedent(description),
                genericParameters,
                annotations
            }
        ),
        createType: (name, genericArguments) => (
            { nodeType: "type", name, genericArguments }
        ),
        createArgument: (name, type, description, annotations) => (
            {
                nodeType: "argument",
                name,
                type,
                description: dedent(description),
                annotations
            }
        ),
        createPackage: function(name, statements) {
            let imports = statements.filter(statement => statement.nodeType === "import")
            let declarations = statements.filter(statement =>
                declarationNodeTypes.indexOf(statement.nodeType) !== -1)

            return { nodeType: "package", name, declarations, imports }
        },
        createImport: (name, alias) => (
            { nodeType: "import", name, alias }
        ),
        createParameter: (name) => (
            { nodeType: "parameter", name }
        ),
        createAnnotation: (name) => (
            { nodeType: "annotation", name }
        )
    }
    
    let none = nodeFactory.createType("none", [])
    let any = nodeFactory.createType("any", [])
}

start
    = "package" __ name:fullName "\n"? statements:statements {
        return nodeFactory.createPackage(name, statements)
    }

fullName
    = head:Id "." tail:fullName { return head + "." + tail }
    / Id

statements
	= statements:(statement)* {
        return [].concat.apply([], statements)
    }

statement
	= _ annotations:annotations? "class" __ name:Id
            genericParameters:("<" genericParameters ">")?
            ancestors:(__ "implements" __ superTypes)?
            description:statementDescription?
            members:classMember* {

        return nodeFactory.createClass(
            name,
            members,
            ancestors === null ? [] : ancestors[3],
            description === null ? "" : description,
            genericParameters === null ? [] : genericParameters[1],
            annotations === null ? [] : annotations
        )
    }
    / _ annotations:annotations? "interface" __ name:Id
            genericParameters:("<" genericParameters ">")?
            ancestors:(__ "extends" __ superTypes)?
            description:statementDescription?
            members:interfaceMember* {
        return nodeFactory.createInterface(
            name,
            members,
            ancestors === null ? [] : ancestors[3],
            description === null ? "" : description,
            genericParameters === null ? [] : genericParameters[1],
            annotations === null ? [] : annotations
        )
    }
    / _ import_:import_ { return import_ }

import_
    = "import" __ entries:importEntries {
        return entries.map(entry => nodeFactory.createImport(entry.name,
                entry.alias))
    }
    / "from" __ packageName:fullName __ "import" __ entries:importEntries {
        return entries.map(entry =>
            nodeFactory.createImport(`${packageName}.${entry.name}`, entry.alias))
    }

importEntries
    = head:importEntry "," __ tail:importEntries { return [head, ...tail] }
    / node:importEntry { return [node] }

importEntry
    = name:fullName __ "as" __ alias:Id { return { name, alias } }
    / name:fullName { return { name, alias: _.last(name.split(".")) } }

annotations = head:annotation __n tail:annotations {
                return [head, ...tail]
            }
            / node:annotation _ {
                return [node]
            }

annotation = "@" name:Id { return nodeFactory.createAnnotation(name) }

superTypes
    = head:type "," __ tail:superTypes { return [head, ...tail] }
    / node:type { return [node] }

statementDescription
    = Indent '"""' text:DescriptionText '"""' { return text }

memberDescription
    = Indent '    """' text:DescriptionText '"""' { return text } 

paramDescription
    = Indent '        """' text:DescriptionText '"""' { return text }
    
classMember
    = Indent member:dependency { return member }
    / Indent member:method { return member }

interfaceMember
    = Indent member:method { return member }
    
dependency
    = _ annotations:annotations? "dependency" __ name:Id type:(":" __ type)? description:memberDescription? {
    	return nodeFactory.createDependency(
            name,
            type === null ? any : type[2],
            description === null ? "" : description,
            annotations === null ? [] : annotations
        )
    }
    
method
    = _ annotations:annotations? "method" __ name:Id
        genericParameters:("<" genericParameters ">")?
        type:(":" __ type)?
        description:memberDescription?
        parameters:param* {
        return nodeFactory.createMethod(
            name,
            parameters === null ? [] : parameters,
            type === null ? none : type[2],
            description === null ? "" : description,
            genericParameters === null ? [] : genericParameters[1],
            annotations === null ? [] : annotations
        )
    }

param
    = _ annotations:annotations? "param" __ name:Id type:(":" __ type)? description:paramDescription? {
        return nodeFactory.createArgument(
            name,
            type === null ? any : type[2],
            description === null ? "" : description,
            annotations === null ? [] : annotations
        )
    }

type
    = name:fullName "<" genericArguments:genericArguments ">" {
        return nodeFactory.createType(name, genericArguments)
    }
    / name:fullName {
        return nodeFactory.createType(name, [])
    }

genericArguments
    = head:type _ "," _ tail:genericArguments { return [head, ...tail] }
    / node:type { return [node] }

genericParameters
    = head:Id _ "," _ tail:genericParameters { return [nodeFactory.createParameter(head), ...tail] }
    / node:Id _ { return [nodeFactory.createParameter(node)] }

Id "identifier"
    = [A-Za-z][A-Za-z0-9]* { return text() }

_ = [ \t\r\n]*
__ = [ \t]+
__n = [ \t\r\n]+

Indent = [ \t\r]*[\n]+"    "
Indent2 = [ \t\r]*[\n]+"        "

DescriptionText = [^"""]* { return text() }
